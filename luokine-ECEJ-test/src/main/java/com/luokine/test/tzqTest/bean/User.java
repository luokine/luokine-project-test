package com.luokine.test.tzqTest.bean;

import lombok.Data;

/**
 * @author: tianziquan
 * @create: 2019-07-10 11:28
 */
@Data
public class User {
    private String name;
    private Integer age;
    private Integer no;
}
