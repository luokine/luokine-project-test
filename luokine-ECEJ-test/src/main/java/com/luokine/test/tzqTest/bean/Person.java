package com.luokine.test.tzqTest.bean;

import lombok.Data;

import java.util.List;

/**
 * @author: tianziquan
 * @create: 2019-07-18 11:16
 */
@Data
public class Person {
    private String perName;
    List<User> userList;
}
