package com.luokine.test.tzqTest.bean;

import lombok.Data;

/**
 * @author: tianziquan
 */
@Data
public class EmpTest {
    private Integer id;

    private String nameTest;

    private Integer ageTest;

    private Byte flag;
}
