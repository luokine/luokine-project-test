package com.luokine.test.tzqTest.bean;

import lombok.Data;

/**
 * @author: tianziquan
 */
@Data
public class EmpTestTwo {
    private Integer id;

    private String name;

    private Integer age;

    private Byte flag;
}
