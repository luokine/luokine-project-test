package com.luokine.test.tzqTest.test.list;

import com.google.common.collect.Lists;
import com.luokine.test.tzqTest.bean.Cup;
import org.junit.Test;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: tianziquan
 * @create: 2019-12-26 11:37
 */
public class RecursiveTest {

    @Test
    public void test() {
        Cup cup = new Cup();
        cup.setName("luokine`s cup");
        cup.setHeigh(3);
        cup.setWidth(3);
        List<Cup> cupList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Cup cups = new Cup();
            cups.setWidth(i);
            cups.setHeigh(i);
            cups.setName("cup" + i);
            cupList.add(cups);
        }
        List<Cup> cupUpdateList = Lists.newArrayList();
        RecursiveTest recursiveTest = new RecursiveTest();
//        recursiveTest.testRecursive(cupList, cupUpdateList, 0);
        testRecursive2(cupList,cupUpdateList,cup);
        System.out.println("cupList = " + cupList);
        System.out.println("cupUpdateList = " + cupUpdateList);
    }


    public void testRecursive(List<Cup> cupList, List<Cup> cupUpdateList, Integer i) {
        if (cupList.size() <= 0) {
            return;
        }
        Cup cup1 = cupList.get(0);
        cupUpdateList.add(cup1);
        cupList.remove(0);
        testRecursive(cupList, cupUpdateList, i++);
    }

    public static void testRecursive2(List<Cup> cupList, List<Cup> cupUpdateList, Cup cup) {
        if (cupUpdateList.size() > 0) {
            return;
        }
        List<Cup> addList = new ArrayList<>();
        cupList.forEach(item -> {
            if (item.getHeigh() == cup.getHeigh()) {
                Cup cup1 = new Cup();
                BeanUtils.copyProperties(item, cup1);
                addList.add(cup1);
            }
        });
        cupUpdateList.addAll(addList);
        testRecursive2(cupList, addList, cup);
    }

    @Test
    public void testThis() {
        int a = 5;
        int b = 10;
        temp(a, b);
        strTemp(a, b);
        System.out.println("temp: a = " + a + ";b = " + b);
        a = a ^ b;
        b = a ^ b;
        a = a ^ b;
        System.out.println("temp ^: a = " + a + ";b = " + b);
    }

    public static void strTemp(int a, int b) {
        a = a ^ b;
        b = a ^ b;
        a = a ^ b;
        System.out.println("strTemp: a = " + a + ";b = " + b);

    }

    public static void temp(int a, int b) {
        int temp = a;
        a = b;
        b = temp;
        System.out.println("temp: a = " + a + ";b = " + b);

    }
}
