package com.luokine.test.tzqTest.test.list;

import com.google.common.collect.Lists;
import com.luokine.common.exception.BaseException;
import com.luokine.test.tzqTest.bean.Emp;
import org.junit.Test;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author: tianziquan
 */
public class OptionalTest {

    @Test
    public void testOptional(){
        List<Emp> empList = Lists.newArrayList();
        Emp emp = new Emp();
        emp.setId(1);
        emp.setAge(23);
        emp.setName("luokine");
        empList.add(emp);
        Emp emp2 = new Emp();
        emp2.setId(2);
        emp2.setAge(24);
        emp2.setName("tian");
        empList.add(emp2);
        System.out.println("empList = " + empList);
        Optional<Emp> first = empList.stream().filter(item -> item.getId().equals(3)).findFirst();
        System.out.println("first = " + first);
        Emp test = first.orElse(new Emp(1, "test", 1));
        System.out.println("test = " + test);
        Emp error = first.orElseThrow(() -> new BaseException(-201, "error"));
        System.out.println("error = " + error);
    }

}
