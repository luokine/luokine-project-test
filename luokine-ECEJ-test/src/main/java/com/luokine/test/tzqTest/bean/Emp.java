package com.luokine.test.tzqTest.bean;

import lombok.Data;

/**
 * @author: tianziquan
 */
@Data
public class Emp {
    private Integer id;

    private String name;

    private Integer age;

    public Emp() {
    }

    public Emp(Integer id, String name, Integer age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }
}
