package com.luokine.test.tzqTest.bean;

import lombok.Data;

import java.util.Date;

/**
 * @author: tianziquan
 * @create: 2019-08-09 11:31
 */
@Data
public class Employee {
    private Long id;
    private String name;
    private Integer age;
//    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    private Date re;

}
